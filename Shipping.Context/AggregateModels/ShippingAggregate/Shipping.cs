﻿using Framework.Domain;
using Shipping.Context.AggregateModels.ShippingAggregate.SyncedEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shipping.Context.AggregateModels.ShippingAggregate
{
    public class ShippingOrder : Entity, IAggregateRoot
    {
        private string _vendorName;
        private string _vendorPhone;
        private decimal _price;
        private DateTime _shippingDate;
        public Order Order { get; private set; }
        private int _orderId { get; set; }
        private ShippingStatus _status;

        public ShippingOrder(string vendorName, string vendorPhone, decimal price, int orderId)
        {
            _vendorName = vendorName;
            _vendorPhone = vendorPhone;
            _price = price;
            _orderId = orderId;
            _shippingDate = DateTime.Now;
        }

        public void UpdateShipping(string vendorName, string vendorPhone, decimal price, int orderId)
        {
            _vendorName = vendorName;
            _vendorPhone = vendorPhone;
            _price = price;
            _orderId = orderId;
        }

        public void SetInProgress()
        {
            _status = ShippingStatus.InProgress;
        }

        public void SetShipped()
        {
            _status = ShippingStatus.Shipped;
        }

        public void SetCancelled()
        {
            _status = ShippingStatus.Cancelled;
        }
    }
}
