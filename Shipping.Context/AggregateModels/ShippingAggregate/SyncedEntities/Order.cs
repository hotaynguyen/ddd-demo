﻿using Framework.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shipping.Context.AggregateModels.ShippingAggregate.SyncedEntities
{
    public class Order : Entity
    {
        public DateTime OrderDate { get; set; }
        public Address Address { get; private set; }
        public string Description { get; set; }
        public ICollection<OrderItem> OrderItems { get; set; }
    }
}
