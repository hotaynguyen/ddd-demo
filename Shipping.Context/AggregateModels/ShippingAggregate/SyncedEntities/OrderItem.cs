﻿using Framework.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shipping.Context.AggregateModels.ShippingAggregate.SyncedEntities
{
    public class OrderItem : Entity
    {
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        public int Units { get; set; }
        public int OrderId { get; set; }
    }
}
