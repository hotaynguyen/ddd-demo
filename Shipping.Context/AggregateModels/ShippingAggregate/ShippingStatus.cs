﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shipping.Context.AggregateModels.ShippingAggregate
{
    public enum ShippingStatus
    {
        InProgress = 1,        
        Shipped = 2,
        Cancelled = 3,
    }
}
