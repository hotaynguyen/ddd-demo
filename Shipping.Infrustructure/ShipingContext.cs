﻿using Framework.Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Shipping.Context.AggregateModels.ShippingAggregate;
using Shipping.Infrustructure.EntityConfigurations;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Shipping.Infrustructure
{
    public class ShipingContext : DbContext
    {
        private readonly IMediator _mediator;
        public DbSet<ShippingOrder> ShippingOrders { get; set; }

        public ShipingContext(DbContextOptions<ShipingContext> options, IMediator mediator)
        : base(options)
        {
            _mediator = mediator;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ShippingEntityTypeConfiguration());
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            int result = await base.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

            // ignore events if no dispatcher provided
            if (_mediator == null) return result;

            var domainEntities = ChangeTracker
                                .Entries<Entity>()
                                .Where(x => x.Entity.DomainEvents != null && x.Entity.DomainEvents.Any());

            var domainEvents = domainEntities
                .SelectMany(x => x.Entity.DomainEvents)
                .ToList();

            domainEntities.ToList()
                .ForEach(entity => entity.Entity.ClearDomainEvents());

            foreach (var domainEvent in domainEvents)
                await _mediator.Publish(domainEvent);

            return result;
        }
    }
}
