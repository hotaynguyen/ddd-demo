﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Shipping.Context.AggregateModels.ShippingAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shipping.Infrustructure.EntityConfigurations
{
    class ShippingEntityTypeConfiguration : IEntityTypeConfiguration<ShippingOrder>
    {
        public void Configure(EntityTypeBuilder<ShippingOrder> orderConfiguration)
        {
            orderConfiguration.HasKey(s => s.Id);
            orderConfiguration.HasOne(s => s.Order);
        }
    }
}
