﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ordering.Context.AggregateModels.OrderAggregate
{
    public enum OrderStatus
    {
        InProgress = 1,
        Shipped = 2,
        Cancelled = 3
    }
}
