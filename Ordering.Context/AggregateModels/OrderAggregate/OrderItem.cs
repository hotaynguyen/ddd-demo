﻿using Framework.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ordering.Context.AggregateModels.OrderAggregate
{
    public class OrderItem : Entity
    {
        private string _productName;
        private decimal _price;
        private decimal _discount;
        private int _units;

        public OrderItem(string productName, decimal price, decimal discount, int units)
        {
            _productName = productName;
            _price = price;
            _discount = discount;
            _units = units;
        }

        public string GetOrderItemProductName() => _productName;

        public decimal GetCurrentDiscount()
        {
            return _discount;
        }

        public int GetUnits()
        {
            return _units;
        }

        public decimal GetPrice()
        {
            return _price;
        }

        public void SetNewDiscount(decimal discount)
        {
            if (discount < 0)
            {
                //throw new OrderingDomainException("Discount is not valid");
            }

            _discount = discount;
        }

        public void AddUnits(int units)
        {
            if (units < 0)
            {
                //throw new OrderingDomainException("Invalid units");
            }

            _units += units;
        }
    }
}
