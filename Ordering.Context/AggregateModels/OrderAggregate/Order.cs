﻿using Framework.Domain;
using Ordering.Context.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ordering.Context.AggregateModels.OrderAggregate
{
    public class Order : Entity, IAggregateRoot
    {
        private DateTime _orderDate;
        public Address Address { get; private set; }
        private string _description;
        private readonly List<OrderItem> _orderItems;
        public IReadOnlyCollection<OrderItem> OrderItems => _orderItems;
        private OrderStatus _status;
        public Order()
        {
            _orderItems = new List<OrderItem>();
        }
        public Order(Address address, string description)
        {
            _orderDate = DateTime.UtcNow;
            Address = address;
            _description = description;
        }
        public void AddOrderItem(string productName, decimal price, decimal discount, int units)
        {
            var existingItem = _orderItems.SingleOrDefault(x => string.Compare(productName, x.GetOrderItemProductName(), true) == 0);

            if(existingItem == null)
            {
                _orderItems.Add(new OrderItem(productName, price, discount, units));
            }
            else
            {
                if (discount > existingItem.GetCurrentDiscount())
                {
                    existingItem.SetNewDiscount(discount);                    
                }
                existingItem.AddUnits(units);
            }
        }
        public void SetCancelledStatus()
        {
            _description = $"The order was cancelled.";
            _status = OrderStatus.Cancelled;
        }
        public void SetInProgressStatus()
        {
            _description = $"The order was shipped.";
            _status = OrderStatus.InProgress;
        }
        public void SetShippedStatus()
        {
            _description = $"The order was shipped.";
            _status = OrderStatus.Shipped;
            AddDomainEvent(new OrderStartedDomainEvent(this));
        }
    }
}
