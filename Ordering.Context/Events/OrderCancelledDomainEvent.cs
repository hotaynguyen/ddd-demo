﻿using MediatR;
using Ordering.Context.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ordering.Context.Events
{
    public class OrderCancelledDomainEvent : INotification
    {
        public OrderEventModel Order { get; }

        public OrderCancelledDomainEvent(OrderEventModel order)
        {
            Order = order;
        }
    }
}
