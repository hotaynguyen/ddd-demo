﻿using MediatR;
using Ordering.Context.AggregateModels.OrderAggregate;
using Ordering.Context.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ordering.Context.Events
{
    public class OrderStartedDomainEvent : INotification
    {
        public Order Order { get; }

        public OrderStartedDomainEvent(Order order)
        {
            Order = order;
        }
    }
}
