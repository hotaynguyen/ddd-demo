﻿using Ordering.Context.AggregateModels.OrderAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ordering.Context.Models
{
    public class OrderEventModel
    {
        public int Id { get; }
        public OrderStatus Status { get; }

        public OrderEventModel(int id, OrderStatus status)
        {
            Id = id;
            Status = status;
        }
    }
}
