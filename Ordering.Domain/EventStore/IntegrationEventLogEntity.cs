﻿using Framework.Domain.EventStore.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Framework.Domain.EventStore
{
    public class IntegrationEventLogEntity
    {
        public Guid EventId { get; private set; }
        public DateTime CreationTime { get; private set; }
        public string EventTypeName { get; private set; }
        public string Content { get; private set; }
        public EventStateEnum State { get; set; }
        public IntegrationEvent IntegrationEvent { get; private set; }
        public string EventTypeShortName => EventTypeName.Split('.')?.Last();

        private IntegrationEventLogEntity() { }

        public IntegrationEventLogEntity(IntegrationEvent @event)
        {
            EventId = @event.Id;
            CreationTime = @event.CreationDate;
        }
        public IntegrationEventLogEntity DeserializeJsonContent(Type type)
        {
            IntegrationEvent = JsonSerializer.Deserialize(Content, type, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true }) as IntegrationEvent;
            return this;
        }
    }
}
