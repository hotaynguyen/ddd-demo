﻿using Framework.Domain.EventStore.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Domain.EventStore.Services
{
    public interface IIntegrationEventLogService
    {
        Task SaveEventAsync(IntegrationEvent @event);
        Task<IEnumerable<IntegrationEventLogEntity>> RetrieveEventLogsPendingToPublishAsync();
        public Task MarkEventAsPublishedAsync(Guid eventId);
        public Task MarkEventAsInProgressAsync(Guid eventId);
        public Task MarkEventAsFailedAsync(Guid eventId);
    }
}
