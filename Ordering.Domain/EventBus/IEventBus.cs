﻿using Framework.Domain.EventStore.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Domain.EventBus
{
    public interface IEventBus
    {
        void Publish(IntegrationEvent @event);
    }
}
