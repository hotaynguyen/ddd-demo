﻿using Microsoft.Extensions.ObjectPool;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ordering.Infrastructure.Messaging
{
    public class RabbitModelPooledObjectPolicy : IPooledObjectPolicy<IModel>
    {
        private readonly IConnection _connection;

        public RabbitModelPooledObjectPolicy(
          IOptions<RabbitMqConfiguration> rabbitMqOptions)
        {
            _connection = GetConnection(rabbitMqOptions.Value);
        }

        public IModel Create()
        {
            return _connection.CreateModel();
        }

        public bool Return(IModel obj)
        {
            if (obj.IsOpen)
            {
                return true;
            }
            else
            {
                obj?.Dispose();
                return false;
            }
        }

        private IConnection GetConnection(RabbitMqConfiguration settings)
        {
            var factory = new ConnectionFactory()
            {
                HostName = settings.Hostname,
                UserName = settings.UserName,
                Password = settings.Password,
                Port = settings.Port,
            };

            return factory.CreateConnection();
        }
    }
}
