﻿using Framework.Domain.EventStore.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ordering.Infrastructure.Messaging
{
    public interface IMessagePublisher
    {
        void Publish(IntegrationEvent @event);
    }
}
