﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ordering.Infrastructure.Messaging
{
    public class RabbitMqConfiguration
    {
        public string Hostname { get; set; }
        public string QueueName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public bool Enabled { get; set; }

        public override string ToString()
        {
            return $"RabbitMQConfiguration:" +
              $"HostName: {Hostname}" +
              $"QueueName: {QueueName}" +
              $"UserName: {UserName}" +
              $"Password: {Password}" +
              $"Port: {Port}" +
              $"Enabled: {Enabled}";
        }
    }
}
