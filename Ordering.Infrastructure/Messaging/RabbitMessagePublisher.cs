﻿using Framework.Domain.EventStore.Events;
using Microsoft.Extensions.ObjectPool;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Ordering.Infrastructure.Messaging
{
    public class RabbitMessagePublisher : IMessagePublisher
    {
        private readonly DefaultObjectPool<IModel> _objectPool;
        public RabbitMessagePublisher(IPooledObjectPolicy<IModel> objectPolicy)
        {
            _objectPool = new DefaultObjectPool<IModel>(objectPolicy, Environment.ProcessorCount * 2);
        }
        public void Publish(IntegrationEvent @event)
        {
            var channel = _objectPool.Get();
            var eventName = @event.GetType().Name;

            try
            {
                string exchangeName = "Ordering";
                channel.ExchangeDeclare(exchangeName, "direct", true, false, null);

                var messageString = JsonSerializer.Serialize(@event);
                var sendBytes = Encoding.UTF8.GetBytes(messageString);

                var properties = channel.CreateBasicProperties();
                properties.Persistent = true;

                channel.BasicPublish(
                  exchange: exchangeName,
                  routingKey: eventName,
                  basicProperties: properties,
                  body: sendBytes);
            }
            finally
            {
                _objectPool.Return(channel);
            }
        }
    }
}
