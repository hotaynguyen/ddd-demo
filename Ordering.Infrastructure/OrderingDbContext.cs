﻿using Framework.Domain;
using Framework.Domain.EventStore.Services;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Ordering.Context.AggregateModels.OrderAggregate;
using Ordering.Infrastructure.EntityConfigurations;
using Ordering.Infrastructure.Messaging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ordering.Infrastructure
{
    public class OrderingDBContext : DbContext, IUnitOfWork
    {
        private readonly IMediator _mediator;
        private readonly IIntegrationEventLogService _eventLogService;
        private readonly IMessagePublisher _messagePublisher;
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public OrderingDBContext(DbContextOptions<OrderingDBContext> options, 
            IMediator mediator,
            IIntegrationEventLogService eventLogService,
            IMessagePublisher messagePublisher)
        : base(options)
        {
            _mediator = mediator;
            _eventLogService = eventLogService;
            _messagePublisher = messagePublisher;
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new OrderEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderItemEntityTypeConfiguration());
        }

        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
        {
            await _mediator.DispatchDomainEventsAsync(this);

            // After executing this line all the changes (from the Command Handler and Domain Event Handlers) 
            // performed through the DbContext will be committed
            var result = await base.SaveChangesAsync(cancellationToken);

            await PublishEventsAsync();

            return true;
        }
        private async Task PublishEventsAsync()
        {
            var pendingLogEvents = await _eventLogService.RetrieveEventLogsPendingToPublishAsync();

            foreach(var pendingLogEvent in pendingLogEvents)
            {
                try
                {
                    await _eventLogService.MarkEventAsInProgressAsync(pendingLogEvent.EventId);
                    _messagePublisher.Publish(pendingLogEvent.IntegrationEvent);
                    await _eventLogService.MarkEventAsPublishedAsync(pendingLogEvent.EventId);
                }
                catch
                {
                    await _eventLogService.MarkEventAsFailedAsync(pendingLogEvent.EventId);
                }
            }
        }
    }
}
