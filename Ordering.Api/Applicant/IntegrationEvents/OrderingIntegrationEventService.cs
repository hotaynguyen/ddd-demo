﻿using Framework.Domain.EventStore.Events;
using Framework.Domain.EventStore.Services;
using Ordering.Infrastructure;
using Ordering.Infrastructure.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ordering.Api.Applicant.IntegrationEvents
{
    public class OrderingIntegrationEventService : IOrderingIntegrationEventService
    {
        private readonly IIntegrationEventLogService _eventLogService;
        private readonly IMessagePublisher _messagePublisher;

        public OrderingIntegrationEventService(IIntegrationEventLogService eventLogService,
            IMessagePublisher messagePublisher)
        {
            _eventLogService = eventLogService;
            _messagePublisher = messagePublisher;
        }

        public async Task AddAndSaveEventAsync(IntegrationEvent evt)
        {
            await _eventLogService.SaveEventAsync(evt);
        }
    }
}
