﻿using Framework.Domain.EventStore.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ordering.Api.Applicant.IntegrationEvents
{
    public interface IOrderingIntegrationEventService
    {
        Task AddAndSaveEventAsync(IntegrationEvent evt);
    }
}
