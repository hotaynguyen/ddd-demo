﻿using Framework.Domain.EventStore.Events;
using Ordering.Api.Applicant.Commands;
using Ordering.Context.AggregateModels.OrderAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ordering.Api.Applicant.IntegrationEvents.Events
{
    public class ShipOrderIntegrationEvent : IntegrationEvent
    {
        public string Country { get; }
        public string City { get; }
        public string Street { get; }
        public List<OrderItemDTO> OrderItems { get; }
        public ShipOrderIntegrationEvent()
        {
        }
        public ShipOrderIntegrationEvent(string country, string city, string street, List<OrderItemDTO> orderItems)
        {
            Country = country;
            City = city;
            Street = street;
            OrderItems = orderItems;
        }
    }
}
