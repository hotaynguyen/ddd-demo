﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ordering.Api.Applicant.Commands
{
    public class CreateOrderCommand : IRequest<bool>
    {
        public string Country { get; private set; }
        public string City { get; private set; }
        public string Street { get; private set; }
        public string Description { get; private set; }
        private readonly List<OrderItemDTO> _orderItems;
        public List<OrderItemDTO> OrderItems => _orderItems;
    }

    public record OrderItemDTO
    {
        public string ProductName { get; init; }
        public decimal Price { get; init; }
        public decimal Discount { get; init; }
        public int Units { get; init; }
    }
}
