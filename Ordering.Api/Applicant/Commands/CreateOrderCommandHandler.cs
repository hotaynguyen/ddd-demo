﻿using MediatR;
using Ordering.Context.AggregateModels.OrderAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Ordering.Api.Applicant.Commands
{
    public class CreateOrderCommandHandler : IRequestHandler<CreateOrderCommand, bool>
    {
        private readonly IOrderRepository _orderRepository;
        public CreateOrderCommandHandler(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }  
        public async Task<bool> Handle(CreateOrderCommand message, CancellationToken cancellationToken)
        {
            var address = new Address(message.Country, message.City, message.Street);
            var order = new Order(address, message.Description);

            foreach(var item in message.OrderItems)
            {
                order.AddOrderItem(item.ProductName, item.Price, item.Discount, item.Units);
            }
            order.SetShippedStatus();

            _orderRepository.Add(order);
            return await _orderRepository.UnitOfWork
                .SaveEntitiesAsync(cancellationToken);
        }
    }
}
