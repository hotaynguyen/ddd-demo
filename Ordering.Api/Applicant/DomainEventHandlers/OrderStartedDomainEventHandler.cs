﻿using MediatR;
using Ordering.Api.Applicant.Commands;
using Ordering.Api.Applicant.IntegrationEvents;
using Ordering.Api.Applicant.IntegrationEvents.Events;
using Ordering.Context.AggregateModels.OrderAggregate;
using Ordering.Context.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Ordering.Api.Applicant.DomainEventHandlers
{
    public class OrderStartedDomainEventHandler : INotificationHandler<OrderStartedDomainEvent>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderingIntegrationEventService _orderingIntegrationEventService;
        public OrderStartedDomainEventHandler(IOrderRepository orderRepository,
            IOrderingIntegrationEventService orderingIntegrationEventService)
        {
            _orderRepository = orderRepository;
            _orderingIntegrationEventService = orderingIntegrationEventService;
        }
        public async Task Handle(OrderStartedDomainEvent orderStartedDomainEvent, CancellationToken cancellationToken)
        {
            var order = await _orderRepository.GetAsync(orderStartedDomainEvent.Order.Id);
            var orderItems = order.OrderItems.Select(x => new OrderItemDTO
            {
                ProductName = x.GetOrderItemProductName(),
                Price = x.GetPrice(),
                Discount = x.GetCurrentDiscount(),
                Units = x.GetUnits()
            }).ToList();
            var shipOrderIntegrationEvent = new ShipOrderIntegrationEvent(order.Address.Country, order.Address.City, order.Address.Street, orderItems);
            await _orderingIntegrationEventService.AddAndSaveEventAsync(shipOrderIntegrationEvent);
        }
    }
}
